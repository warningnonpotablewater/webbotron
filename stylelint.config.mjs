export default {
    "extends": "stylelint-config-html",
    "plugins": ["stylelint-plugin-logical-css"],

    "rules": {
        "plugin/use-logical-properties-and-values": [
            true,
            {"severity": "error"},
        ],
        "plugin/use-logical-units": [true, {"severity": "error"}],
    }
};
