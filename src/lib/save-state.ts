type ObjectLike = Record<string, unknown>;

export function deserialize<T extends ObjectLike>(oldParameters: T) {
    const hash = location.hash;
    const encoded = hash.slice(1);
    const json = decodeURIComponent(encoded);

    let newParameters: T;

    try {
        newParameters = JSON.parse(json);
    } catch (_) {
        return;
    }

    if (!isObjectLike(newParameters)) {
        return;
    }

    for (const property of Object.keys(newParameters)) {
        if (!Object.hasOwn(oldParameters, property)) {
            return;
        }

        if (!typeCheck(newParameters[property], oldParameters[property])) {
            return;
        }
    }

    return newParameters;
};

export function serialize(parameters: ObjectLike) {
    const json = JSON.stringify(parameters);
    const encoded = encodeURIComponent(json);

    const url = new URL(location.href);

    url.hash = `#${encoded}`;

    return url.href;
};

function typeCheck(userValue: unknown, appValue: unknown) {
    const userType = valueType(userValue);
    const appType = valueType(appValue);

    if (userType != appType) {
        return false;
    }

    if (isObjectLike(userValue) && isObjectLike(appValue)) {
        const userProperties = Object.keys(userValue);
        const appProperties = Object.keys(appValue);

        if (userProperties.length != appProperties.length) {
            return false;
        }

        for (const property of userProperties) {
            if (!appValue.hasOwnProperty(property)) {
                return false;
            }

            if (!typeCheck(userValue[property], appValue[property])) {
                return false;
            }
        }
    }

    return true;
}

function valueType(value: unknown) {
    if (value === null) {
        return "null";
    } else if (isObjectLike(value)) {
        return "object";
    } else if (Array.isArray(value)) {
        return "array";
    } else {
        return typeof value;
    }
}

function isObjectLike(value: unknown): value is ObjectLike {
    return value != null && typeof value == "object" && !Array.isArray(value);
}
