import us from "../../assets/us.csv?url";
import fr from "../../assets/fr.csv?url";

import {Name} from "./name";

class Dataset {
    cache?: Name[];

    constructor(
        public url: string
    ) {}
}

const datasets = {
    "United States, 1880 to 2023": new Dataset(us),
    "France, 1900 to 2021": new Dataset(fr),
};

export const all = Object.keys(datasets) as keysOf<typeof datasets>;
export const main = all[0];

export async function load(key: keyof typeof datasets) {
    const dataset = datasets[key] ?? main;

    if (dataset.cache) {
        return dataset.cache;
    }

    const response = await fetch(dataset.url);

    if (!response.ok) {
        throw new Error("HTTP server couldn't deliver the dataset");
    }

    const csv = await response.text();
    const names = parse(csv);

    dataset.cache = names;

    return names;
};

function parse(csv: string) {
    const lines = csv.split(/\r?\n/);
    const nonEmptyLines = lines.filter((line: string) => line != "");
    const rows = nonEmptyLines.map((line: string) => line.split(","));

    const toFields = (column: string, index: number) => {
        if (index == 0) {
            return column;
        }

        return Number(column);
    };

    const fields = rows.map(
        row => row.map(toFields) as ConstructorParameters<typeof Name>
    );
    const names = fields.map(entry => new Name(...entry));

    return names;
}
