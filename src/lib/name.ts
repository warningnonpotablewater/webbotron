export const FieldType = {
    RELATIVE: "Relative",
    ABSOLUTE: "Absolute",
    EVERYTHING: "Everything"
};

export type FieldType = valueOf<typeof FieldType>;

// Necessary for being able to do name[field] without constantly casting
// everything to number.

export type Field = Exclude<keyof Name, "_index">;
export type NumericField = Exclude<Field, "spelling">;

export const SortOrder = {
    DESCENDING: "Descending",
    ASCENDING: "Ascending",
};

export type SortOrder = valueOf<typeof SortOrder>;

// Has to be an object and not a reactive class in order to be serializable.

export const Limit = (min: number, max: number): Limit => ({min, max});

export interface Limit {
    min: number,
    max: number,
};

export class Name {
    _index?: Fuzzysort.Prepared;

    spelling: string;

    bestYear: number;
    femPercent: number;
    rarePercent: number;

    femininity: number;
    masculinity: number;
    popularity: number;

    totalCharacters: number;
    totalVowels: number;
    totalConsonants: number;
    consecutiveVowels: number;
    consecutiveConsonants: number;

    constructor(
        spelling: string,

        bestYear: number,
        femPercent: number,
        rarePercent: number,

        femininity: number,
        masculinity: number,
        popularity: number,

        totalCharacters: number,
        totalVowels: number,
        totalConsonants: number,
        consecutiveVowels: number,
        consecutiveConsonants: number,
    ) {
        this.spelling = spelling;

        this.bestYear = bestYear;
        this.femPercent = femPercent;
        this.rarePercent = rarePercent;

        this.femininity = femininity;
        this.masculinity = masculinity;
        this.popularity = popularity;

        this.totalCharacters = totalCharacters;
        this.totalVowels = totalVowels;
        this.totalConsonants = totalConsonants;
        this.consecutiveVowels = consecutiveVowels;
        this.consecutiveConsonants = consecutiveConsonants;
    }

    static fieldCaptions: Record<Field, string> = {
        spelling: "Name",

        bestYear: "Best year",
        femPercent: "Fem%",
        rarePercent: "Rare%",

        femininity: "Femininity",
        masculinity: "Masculinity",
        popularity: "Popularity",

        totalCharacters: "Length",
        totalVowels: "Total vowels",
        totalConsonants: "Total consonants",
        consecutiveVowels: "Consecutive vowels",
        consecutiveConsonants: "Consecutive consonants",
    };

    static textFields: Field[] = [
        "spelling",
    ];
    static relativeFields: NumericField[] = [
        "bestYear", "femPercent", "rarePercent",
    ];
    static absoluteFields: NumericField[] = [
        "femininity", "masculinity", "popularity",
    ];
    static metaFields: NumericField[] = [
        "totalCharacters", "totalVowels", "totalConsonants",
        "consecutiveVowels", "consecutiveConsonants",
    ];

    static sortableFields: NumericField[] = [
        "bestYear", ...Name.absoluteFields, ...Name.metaFields,
    ];
    static limitableFields: NumericField[] = [
        ...Name.relativeFields, ...Name.metaFields,
    ];

    static defaultSortField: NumericField = "popularity";
    static defaultSortOrder = SortOrder.DESCENDING;

    static defaultLimits = {
        bestYear: Limit(0, 9999),
        femPercent: Limit(0, 100),
        rarePercent: Limit(0, 100),

        totalCharacters: Limit(1, 100),
        totalVowels: Limit(0, 100),
        totalConsonants: Limit(0, 100),
        consecutiveVowels: Limit(0, 100),
        consecutiveConsonants: Limit(0, 100),
    } as Record<NumericField, Limit>;

    static fieldsByType: Record<FieldType, Field[]> = {
        [FieldType.RELATIVE]: [
            ...Name.textFields, ...Name.relativeFields,
        ],
        [FieldType.ABSOLUTE]: [
            ...Name.textFields, ...Name.absoluteFields,
        ],
        [FieldType.EVERYTHING]: [
            ...Name.textFields, ...Name.relativeFields, ...Name.absoluteFields,
            ...Name.metaFields,
        ],
    };

    static captionsForFields(fields: Field[]) {
        return fields.map(field => Name.fieldCaptions[field]);
    }
};
