import type {Name} from "../lib/name";

export function shuffle(names: Name[]) {
    for (let i = names.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));

        [names[i], names[j]] = [names[j], names[i]];
    }

    return names;
};
