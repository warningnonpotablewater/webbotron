import type {Limit, NumericField} from "../lib/name";
import {Name} from "../lib/name";

export function limit(names: Name[], limits: Record<NumericField, Limit>) {
    for (
        const [field, bounds]
        of Object.entries(limits) as entriesOf<typeof limits>
    ) {
        const {min, max} = bounds;

        const defaultLimit = Name.defaultLimits[field];
        const alreadyLimited =
            min == defaultLimit.min
            && max == defaultLimit.max;

        if (alreadyLimited) {
            continue;
        }

        const byField =
            (name: Name) => name[field] >= min && name[field] <= max;

        names = names.filter(byField);
    }

    return names;
};
