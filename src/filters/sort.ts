import type {NumericField} from "../lib/name";
import {Name, SortOrder} from "../lib/name";

export function sort(names: Name[], field: NumericField, order: SortOrder) {
    const alreadySorted =
        field == Name.defaultSortField
        && order == Name.defaultSortOrder;

    if (alreadySorted) {
        return names;
    }

    const byField = (a: Name, b: Name) => {
        const fieldA = a[field];
        const fieldB = b[field];

        if (order == SortOrder.ASCENDING) {
            return fieldA - fieldB;
        } else {
            return fieldB - fieldA;
        }
    };

    return names.toSorted(byField);
};
