import fuzzysort from "fuzzysort";

import type {Name} from "../lib/name";

export function index(names: Name[]) {
    if (names[0]._index) {
        return;
    }

    for (const name of names) {
        name._index = fuzzysort.prepare(name.spelling);
    }
};

export function search(names: Name[], query: string) {
    const results = fuzzysort.go(query, names, {all: true, key: "_index"});
    const toNames = (result: Fuzzysort.KeyResult<Name>) => result.obj;

    return results.map(toNames);
};
