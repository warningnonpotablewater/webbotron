import type {Name} from "../lib/name";

const tokenize = (userInput: string) => userInput
    .split(/[,\s]+/)
    .map(string => string.trim().toLowerCase())
    .filter(string => string != "");

function excludeMorphemes(
    names: Name[],
    userInput: string,
    hasMorpheme: (spelling: string, morpheme: string) => boolean,
) {
    const morphemes = tokenize(userInput);

    if (morphemes.length == 0) {
        return names;
    }

    for (const morpheme of morphemes) {
        const onesWithoutMorpheme = (name: Name) => {
            const normalized = name.spelling.toLowerCase();

            return !hasMorpheme(normalized, morpheme);
        };

        names = names.filter(onesWithoutMorpheme);
    }

    return names;
}

function excludePattern(names: Name[], pattern: string) {
    if (pattern == "") {
        return names;
    }

    try {
        const regexp = new RegExp(pattern, "i");

        return names.filter(name => !regexp.test(name.spelling));
    } catch {
        return names;
    }
}

export function letters(names: Name[], userInput: string) {
    return excludePattern(names, `[${userInput}]`);
};

export function prefixes(names: Name[], userInput: string) {
    const hasPrefix =
        (spelling: string, prefix: string) => spelling.startsWith(prefix);

    return excludeMorphemes(names, userInput, hasPrefix);
};

export function postfixes(names: Name[], userInput: string) {
    const hasPostfix =
        (spelling: string, postfix: string) => spelling.endsWith(postfix);

    return excludeMorphemes(names, userInput, hasPostfix);
};

export function regex(names: Name[], userInput: string) {
    return excludePattern(names, userInput);
};
