/// <reference types="vite/client" />

type valueOf<T> = T[keyof T];

type keysOf<T> = (keyof T)[];
type entriesOf<T> = [keyof T, valueof<T>][];
