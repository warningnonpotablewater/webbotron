import type {Entry} from "./adapt";

// Yes, "y" is a vowel too. No complaints will be accepted.

const VOWELS = "aeiouy";
const CONSONANTS = "bcdfghjklmnpqrstvwxz";

class Record {
    femininity: number;
    masculinity: number;
    popularity: number;

    bestYear: number;
    bestYearPopularity: number;

    constructor(
        femininity: number,
        masculinity: number,
        popularity: number,

        bestYear: number,
        bestYearPopularity: number,
    ) {
        this.femininity = femininity;
        this.masculinity = masculinity;
        this.popularity = popularity;

        this.bestYear = bestYear;
        this.bestYearPopularity = bestYearPopularity;
    }
}

export class Name {
    spelling: string;

    bestYear: number;
    femPercent: number;
    rarePercent: number;

    femininity: number;
    masculinity: number;
    popularity: number;

    totalCharacters: number;
    totalVowels: number;
    totalConsonants: number;
    consecutiveVowels: number;
    consecutiveConsonants: number;

    constructor(
        spelling: string,

        bestYear: number,
        femPercent: number,
        rarePercent: number,

        femininity: number,
        masculinity: number,
        popularity: number,

        totalCharacters: number,
        totalVowels: number,
        totalConsonants: number,
        consecutiveVowels: number,
        consecutiveConsonants: number,
    ) {
        this.spelling = spelling;

        this.bestYear = bestYear;
        this.femPercent = femPercent;
        this.rarePercent = rarePercent;

        this.femininity = femininity;
        this.masculinity = masculinity;
        this.popularity = popularity;

        this.totalCharacters = totalCharacters;
        this.totalVowels = totalVowels;
        this.totalConsonants = totalConsonants;
        this.consecutiveVowels = consecutiveVowels;
        this.consecutiveConsonants = consecutiveConsonants;
    }
};

export function refine(entries: Entry[]) {
    const names = calculateFields(mergeEntries(entries));

    const byRarePercent = (a: Name, b: Name) => a.rarePercent - b.rarePercent;

    return names.toSorted(byRarePercent);
};

function mergeEntries(entries: Entry[]) {
    const records = new Map<string, Record>();

    for (const entry of entries) {
        let femininity = 0;
        let masculinity = 0;

        if (entry.legalGender == "F") {
            femininity = entry.personCount;
        } else {
            masculinity = entry.personCount;
        }

        if (!records.has(entry.spelling)) {
            records.set(entry.spelling, new Record(
                femininity,
                masculinity,
                entry.personCount,

                entry.year,
                entry.personCount,
            ));
        } else {
            const record = records.get(entry.spelling)!;

            record.femininity += femininity;
            record.masculinity += masculinity;
            record.popularity += entry.personCount;

            if (entry.personCount >= record.bestYearPopularity) {
                record.bestYear = entry.year;
                record.bestYearPopularity = entry.personCount;
            }
        }
    }

    return records;
}

function calculateFields(records: Map<string, Record>) {
    const names = [];

    const toPopularity = (record: Record) => record.popularity;
    const recordPopularities = Array.from(records.values()).map(toPopularity);

    const minPopularity = Math.min(...recordPopularities);
    const maxPopularity = Math.max(...recordPopularities);

    const adjustedMaxPopularity = maxPopularity - minPopularity;

    for (const [spelling, record] of records) {
        const adjustedPopularity = record.popularity - minPopularity;

        const femPercent = linearPercentage(
            record.femininity,
            record.popularity,
        );
        const rarePercent = 100 - logPercentage(
            adjustedPopularity,
            adjustedMaxPopularity,
        );

        const totalCharacters = spelling.length;

        const totalVowels = countTotalLetters(spelling, VOWELS);
        const totalConsonants = countTotalLetters(spelling, CONSONANTS);

        const consecutiveVowels = countConsecutiveLetters(
            spelling,
            VOWELS,
        );
        const consecutiveConsonants = countConsecutiveLetters(
            spelling,
            CONSONANTS,
        );

        names.push(new Name(
            spelling,

            record.bestYear,
            femPercent,
            rarePercent,

            record.femininity,
            record.masculinity,
            record.popularity,

            totalCharacters,
            totalVowels,
            totalConsonants,
            consecutiveVowels,
            consecutiveConsonants,
        ));
    }

    return names;
}

function linearPercentage(number: number, threshold: number) {
    return number / threshold * 100;
}

function logPercentage(number: number, threshold: number) {
    if (number <= 0) {
        return 0;
    }

    return Math.log(number) / Math.log(threshold) * 100;
}

function countTotalLetters(spelling: string, letters: string) {
    const matches = findPattern(spelling, `[${letters}]`);

    return Array.from(matches).length;
}

function countConsecutiveLetters(spelling: string, letters: string) {
    const matches = findPattern(spelling, `[${letters}]+`);
    const matchLengths = Array.from(matches).map(match => match[0].length);

    if (matchLengths.length == 0) {
        return 0;
    }

    return Math.max(...matchLengths);
}

function findPattern(spelling: string, pattern: string) {
    const asciiSpelling = stripAccents(spelling);
    const regexp = new RegExp(pattern, "ig");

    return asciiSpelling.matchAll(regexp);
}

function stripAccents(spelling: string) {
    const accents = /\p{Mn}/gu;

    return spelling.normalize("NFKD").replaceAll(accents, "");
}
