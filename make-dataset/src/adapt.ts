type LegalGender = "F" | "M";

export class Entry {
    spelling: string;
    legalGender: LegalGender;
    personCount: number;
    year: number;

    constructor(
        spelling: string,
        legalGender: LegalGender,
        personCount: number,
        year: number,
    ) {
        this.spelling = spelling;
        this.legalGender = legalGender;
        this.personCount = personCount;
        this.year = year;
    }
};

export function us(rows: string[][], filename: string) {
    const year = parseInt(filename.replace(/^yob/, ""));

    return rows.map(row => new Entry(
        row[0],
        row[1] as LegalGender,
        parseInt(row[2]),
        year,
    ));
};

export function fr(rows: string[][], filename: string) {
    const entries = [];

    for (const row of rows) {
        const spelling = row[1];
        const legalGender = row[0] == "2" ? "F" : "M";
        const personCount = row[3];
        const year = row[2];

        const entryIsIncomplete =
            spelling.startsWith("_")
            || personCount == "nombre"
            || year == "XXXX";

        if (entryIsIncomplete) {
            continue;
        }

        entries.push(new Entry(
            titleCase(spelling),
            legalGender as LegalGender,
            parseInt(personCount),
            parseInt(year),
        ));
    }

    return entries;
};

function titleCase(spelling: string) {
    const lowerCase = spelling.toLowerCase();

    const firstLetterOfEveryWord = /(^|\p{P}|\p{Z})\p{L}/gu;
    const toUpperCase = (match: string) => match.toUpperCase();

    return lowerCase.replaceAll(firstLetterOfEveryWord, toUpperCase);
}
