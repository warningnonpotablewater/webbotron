import path from "node:path";
import {argv, exit, stderr, stdout} from "node:process";
import {readFileSync} from "node:fs";

import {parse} from "csv-parse/sync";
import {stringify} from "csv-stringify/sync";

import {us, fr} from "./adapt";
import type {Entry} from "./adapt";
import type {Name} from "./refine";
import {refine} from "./refine";

type Adapter = (rows: string[][], filename: string) => Entry[];

const adapters: Record<string, Adapter> = {us, fr};
const delimiters: Record<string, string> = {
    us: ",",
    fr: ";",
};

main();

function main() {
    if (argv.length < 3) {
        die("usage: <dataset> [files ...]");
    }

    const dataset = argv[2];
    const filenames = argv.slice(3);

    if (!Object.hasOwn(delimiters, dataset)) {
        die(`'${dataset}' is not a valid dataset type`);
    }

    let entries: Entry[] = [];

    for (const filename of filenames) {
        const csv = readFileSync(filename);
        const rows = parse(csv, {delimiter: delimiters[dataset]});
        const normalizedFilename = path.parse(filename).name;

        const newEntries = adapters[dataset](rows, normalizedFilename);

        entries = entries.concat(newEntries);
    }

    const names = refine(entries);

    printAsCsv(names);
}

function die(message: string) {
    stderr.write(message);

    exit(1);
}

function printAsCsv(names: Name[]) {
    const rows = names.map(name => {
        const fields = name as Record<string, any>;

        for (const field of ["femPercent", "rarePercent"]) {
            fields[field] = roundWithoutTrailingZeroes(fields[field]);
        }

        return Object.values(name);
    });

    const csv = stringify(rows, {record_delimiter: "\r\n"});

    stdout.write(csv);
}

function roundWithoutTrailingZeroes(x: number) {
    const PRECISION = 3;

    const magnitude = 10 ** PRECISION;
    const rounded = Math.round(x * magnitude) / magnitude;

    return String(rounded);
}
