#!/bin/sh

set -uex

INPUT=raw
OUTPUT=cooked

US=us.csv
FR=fr.csv

fetch() {
    mkdir -p "${INPUT}"/"${1}"
    cd "${INPUT}"/"${1}"

    wget --no-clobber "${2}"
    unzip -n -- *.zip

    cd ../..
}

dataset() {
    npx tsx src/main.ts "${@}"
}

npx tsc --noEmit

mkdir -p "${OUTPUT}"

fetch "${US}" 'https://www.ssa.gov/oact/babynames/names.zip'
dataset us "${INPUT}"/"${US}"/*.txt > "${OUTPUT}"/"${US}"

fetch "${FR}" 'https://www.insee.fr/fr/statistiques/fichier/2540004/nat2021_csv.zip'
dataset fr "${INPUT}"/"${FR}"/*.csv > "${OUTPUT}"/"${FR}"
