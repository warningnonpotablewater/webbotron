Webbotron is a name picker app for nerds.

## How to build

```
npm install
npm run build
```

## How to debug

```
npm run dev
```

## How to reproduce the dataset

```
cd make-dataset

npm install
./run.sh

cp cooked/*.csv ../assets
```

## Design inspiration

* Firefox's default input styles
* GTK 4's Default theme (not to be confused with Adwaita)
* GitLab's input focus style
* MediaWiki's Vector (2022) theme
* Yahoo! Japan's homepage

## Meaning behind the name

Web + Name-o-Tron 9000 = Webbotron.

## License

[GNU AGPL v3 or later](src/components/About.svelte).
